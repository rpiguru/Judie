# Judie

Judie

## Components

- Raspberry Pi 0 W

    https://shop.pimoroni.com/products/raspberry-pi-zero-w

    (For Development)

    https://shop.pimoroni.com/products/pi-zero-w-starter-kit

- RC522 Module

    https://www.amazon.co.uk/kwmobile-Keychain-Module-Reader-Arduino/dp/B01M4O4V5H/

- 5" Touchscreen from Waveshare

    https://www.waveshare.com/5inch-HDMI-LCD.htm

    https://www.amazon.co.uk/Waveshare-Inch-Resolution-Raspberry-Model/dp/B01HPV7D38


## Wiring Components

| Board pin name | Board pin | Physical RPi pin | RPi pin name |
|----------------|-----------|------------------|--------------|
| SDA            | 1         | 22               | GPIO25, CS   |
| SCK            | 2         | 15               | GPIO22, SCK  |
| MOSI           | 3         | 18               | GPIO24, MOSI |
| MISO           | 4         | 16               | GPIO23, MISO |
| GND            | 6         | 6, 9, 20, 25     | Ground       |
| RST            | 7         | 22               | 3V3          |
| 3.3V           | 8         | 1,17             | 3V3          |


![Diagram](schematic/schematic_bb.jpg "Schematic")


## Installation

- Download the Raspbian Buster **Lite** image and flash your micro sd card:

    https://www.raspberrypi.org/downloads/raspberry-pi-os/

    Follow this guide to configure your RPi 0 W and enable SSH.
    
    https://www.youtube.com/watch?v=LlCr09B2HZI

- Clone our repository and install

    ```shell script
    cd ~
    sudo apt update
    sudo apt install -y git
    git clone https://gitlab.com/rpiguru/Judie
    cd Judie
    bash setup.sh
    ```

- And reboot!  :)
