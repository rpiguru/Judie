import os
import signal
import subprocess
import threading
import time

from settings import PIN_DEFAULT, PIN_READ, CMD, DEFAULT_IMG, SCAN_TIMEOUT
from utils.common import logger, kill_process_by_name, check_running_proc
from utils.rc522 import RC522Reader
import RPi.GPIO as GPIO


GPIO.setup(PIN_DEFAULT, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(PIN_READ, GPIO.IN, pull_up_down=GPIO.PUD_UP)


_cur_dir = os.path.dirname(os.path.realpath(__file__))


class HappyJudie(threading.Thread):

    def __init__(self):
        super().__init__()
        self._b_stop = threading.Event()
        self._b_stop.clear()
        self.rfid = RC522Reader(callback=self._on_rfid_read)
        GPIO.add_event_detect(PIN_DEFAULT, GPIO.FALLING, callback=self._on_btn_default, bouncetime=100)
        GPIO.add_event_detect(PIN_READ, GPIO.FALLING, callback=self._on_btn_read, bouncetime=100)
        self._enabled_time = 0
        self._cur_id = None

    def run(self) -> None:
        self.rfid.start()
        while not self._b_stop.is_set():
            time.sleep(1)

    def _on_rfid_read(self, uid):
        logger.info(f"RFID Read: {uid}")
        if time.time() - self._enabled_time < SCAN_TIMEOUT:
            if self._cur_id == uid and check_running_proc('fbi'):
                logger.info("Already displaying")
            else:
                file_path = os.path.join(_cur_dir, 'images', f"{uid}.jpg")
                if os.path.exists(file_path):
                    logger.info(f"Image found - {file_path}")
                    kill_process_by_name('fbi', sig=signal.SIGTERM)
                    subprocess.Popen(CMD.format(img_path=file_path), shell=True)
                self._cur_id = uid
            self._enabled_time = 0
        else:
            logger.warning(f"Scan timeout, please press SCAN button.")

    def stop(self):
        self.rfid.stop()
        self._b_stop.set()

    def _on_btn_default(self, *args):
        logger.info("DEFAULT button is pressed")
        kill_process_by_name('fbi', sig=signal.SIGTERM)
        subprocess.Popen(CMD.format(img_path=DEFAULT_IMG), shell=True)
        self._cur_id = None

    def _on_btn_read(self, *args):
        logger.info("READ button is pressed.")
        self._enabled_time = time.time()


if __name__ == '__main__':

    logger.info("========== Starting Happy Judie ==========")

    happy_judie = HappyJudie()

    happy_judie.start()

    while True:
        try:
            time.sleep(.1)
        except KeyboardInterrupt:
            break
    happy_judie.stop()
