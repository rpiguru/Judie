import os

_cur_dir = os.path.dirname(os.path.realpath(__file__))


BASE_DIR = os.path.expanduser('~/.jd')
os.makedirs(BASE_DIR, exist_ok=True)

IMG_DIR = os.path.join(BASE_DIR, 'images')
os.makedirs(IMG_DIR, exist_ok=True)

CMD = 'sudo fbi -d /dev/fb0 -T 1 -1 --noverbose --fitwidth --autozoom {img_path}'

DEFAULT_IMG = os.path.join(_cur_dir, 'images', 'default.jpg')

SCAN_TIMEOUT = 3

PIN_DEFAULT = 38
PIN_READ = 40
