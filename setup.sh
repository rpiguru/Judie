echo "Stating Judie Installation."
echo "Installation logs are saved to /home/pi/judie_install.log"

exec &> >(tee -a "/home/pi/judie_install.log")

echo "========== Installing Judie =========="

cur_dir="$( cd "$(dirname "$0")" ; pwd -P )"

sudo apt update

sudo apt install -y python3-dev python3-pip build-essential cmake fbi

sudo pip3 install -U pip
sudo pip3 install -U setuptools wheel
sudo pip3 install -r ${cur_dir}/requirements.txt

#bash ${cur_dir}/setup_wifi.sh

# Enable SPI
sudo echo "dtparam=spi=on" | sudo tee -a /boot/config.txt
echo "gpu_mem=128" | sudo tee -a /boot/config.txt

# Install 5" display driver
sudo echo "max_usb_current=1" | sudo tee -a /boot/config.txt
sudo echo "hdmi_group=2" | sudo tee -a /boot/config.txt
sudo echo "hdmi_mode=87" | sudo tee -a /boot/config.txt
sudo echo "hdmi_cvt 800 480 60 6 0 0 0" | sudo tee -a /boot/config.txt
sudo echo "hdmi_drive=1" | sudo tee -a /boot/config.txt

# Remove login prompt
sudo systemctl disable getty@tty1

# Disable booting logo and messages
sudo echo "disable_splash=1" | sudo tee -a /boot/config.txt
sudo systemctl mask plymouth-start.service
sudo sed -i -- "s/$/ logo.nologo quiet loglevel=3 vt.global_cursor_default=0 systemd.show_status=0 plymouth.ignore-serial-consoles plymouth.enable=0/" /boot/cmdline.txt
sudo sed -i -- "s/console=tty1/console=tty3/g" /boot/cmdline.txt

# Disable some services to reduce booting time
sudo systemctl disable hciuart
sudo echo "dtoverlay=pi3-disable-bt" | sudo tee -a /boot/config.txt
sudo echo "boot_delay=0" | sudo tee -a /boot/config.txt

sudo mkdir /etc/systemd/system/networking.service.d
sudo touch /etc/systemd/system/networking.service.d/reduce-timeout.conf
echo "[Service]" | sudo tee -a /etc/systemd/system/networking.service.d/reduce-timeout.conf
echo "TimeoutStartSec=1" | sudo tee -a /etc/systemd/system/networking.service.d/reduce-timeout.conf
sudo rm /etc/systemd/system/dhcpcd.service.d/wait.conf

# Enable auto start
sudo apt install -y screen

sudo sed -i -- "s/^exit 0/screen -mS jd -d\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/screen -S jd -X stuff \"cd ${cur_dir////\\/}\\\\r\"\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/screen -S jd -X stuff \"python3 main.py\\\\r\"\\nexit 0/g" /etc/rc.local
