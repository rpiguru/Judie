import threading
import time
import RPi.GPIO as GPIO

from pirc522 import RFID


rdr = RFID(bus=0, device=0, pin_irq=12, pin_rst=11)


class RC522Reader(threading.Thread):

    def __init__(self, callback=None):
        super().__init__()
        self.callback = callback
        self._b_stop = threading.Event()
        self._b_stop.clear()

    def run(self) -> None:
        while not self._b_stop.is_set():
            rdr.wait_for_tag()
            error, tag_type = rdr.request()
            if not error:
                error, uid = rdr.anticoll()
                if not error:
                    uuid = ''.join([format(v, '02X') for v in uid])
                    if callable(self.callback):
                        self.callback(str(uuid))
                    # Select Tag is required before Auth
                    # if not rdr.select_tag(uid):
                    #     # Auth for block 10 (block 2 of sector 2) using default shipping key A
                    #     if not rdr.card_auth(rdr.auth_a, 10, [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF], uid):
                    #         # This will print something like (False, [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
                    #         print("Reading block 10: " + str(rdr.read(10)))
            time.sleep(.1)
        rdr.stop_crypto()
        rdr.cleanup()

    def stop(self):
        self._b_stop.set()
        GPIO.cleanup()


if __name__ == '__main__':

    def print_result(result):
        print(result)

    r = RC522Reader(callback=print_result)
    r.start()
